package objects;

/**
 * Zaimplementuj klasę `Line` która będzie zawierać (na zasadzie kompozycji) instancję dwóch obiektów `Point2D` z zadania nr 1.
 * Punkty te będą punktem początkowym oraz końcowym odcinka. Ponadto klasa ta powinna implementować:
 * * konstruktor przyjmujący dwa punkty: początkowy i końcowy
 * * konstruktor przyjmujący 4 parametry: współrzędne punktu początkowego oraz końcowego
 * * metody typu `getter` odpowiedzialne za zwracanie punktów: początkowego i końcowego
 * * metody typu `setter` odpowiedzialne za ustalanie punktów: początkowego i końcowego
 * * metodę odpowiedzialną za obliczanie długości linii na podstawie ustawionych punktów
 * * metodę odpowiedzialną za zwracanie współrzędnych punktu będącego środkiem stworzonej prostej
 * Zaprezentuj zaimplementowane powyżej rozwiązanie na przykładzie.
 */

public class Line {
    Point2D pointA;
    Point2D pointB;

    // konstruktor przyjmujący dwa punkty: początkowy i końcowy
    public Line(Point2D pointA, Point2D pointB) {
        this.pointA = pointA;
        this.pointB = pointB;
    }

    // konstruktor przyjmujący 4 parametry: współrzędne punktu początkowego oraz końcowego
    public Line(float pointACoordinateX, float pointACoordinateY, float pointBCoordinateX, float pointBCoordinateY) {
        pointA = new Point2D(pointACoordinateX, pointACoordinateY);
        pointB = new Point2D(pointBCoordinateX, pointBCoordinateY);
    }

    // metody typu `getter` odpowiedzialne za zwracanie punktów: początkowego i końcowego
    public Point2D getPointA() {
        return pointA;
    }

    public Point2D getPointB() {
        return pointB;
    }


    // metody typu `setter` odpowiedzialne za ustalanie punktów: początkowego i końcowego
    public void setPointA(Point2D pointA) {
        this.pointA = pointA;
    }

    public void setPointB(Point2D pointB) {
        this.pointB = pointB;
    }

    // metodę odpowiedzialną za obliczanie długości linii na podstawie ustawionych punktów
    public float getLineSegmentLength() {
        return (float) Math.sqrt(Math.pow((pointB.getX() - pointA.getX()), 2) + Math.pow((pointB.getY() - pointA.getY()), 2));
    }

    // metodę odpowiedzialną za zwracanie współrzędnych punktu będącego środkiem stworzonej prostej
    public float[] getMiddlePointOfLineSegment() {
        float middlePointCoordinateX = (pointA.getX() + pointB.getX()) / 2;
        float middlePointCoordinateY = (pointA.getY() + pointB.getY()) / 2;
        return new float[]{middlePointCoordinateX, middlePointCoordinateY};
    }
}
