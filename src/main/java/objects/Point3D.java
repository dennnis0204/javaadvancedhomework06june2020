package objects;

/**
 * Zaimplementuj klasę `Point2D`. Klasa powinna zawierać:
 * * pole prywatne typu float: `x`, `y`, `z`
 * * konstruktor przyjmujący wartości dla pól: `x`, `y`, `z`
 * * metody typu getter odpowiedzialne za zwracanie wartości zmiennej: `x`, `y`, `z`
 * * metodę `getXYZ` zwracającą współrzędne `x`, `y`, `z` w postaci tablicy trzyelementowej
 * * metodę setXY ustawiającą współrzędne `x` i `y`, `z`
 * * metodę `setXYZ` ustawiającą wartości dla zmiennych `x`, `y`, `z`
 * * metoda `toString` powinna zwracać łańcuch tekstowy o następującym formacie: `(x, y, z)`;
 * <p>
 * Zaprezentuj zaimplementowane powyżej rozwiązanie na przykładzie.
 */

public class Point3D {

    // pole prywatne typu float: `x`, `y`, `z`
    private float x;
    private float y;
    private float z;

    // konstruktor przyjmujący wartości dla pól: `x`, `y`, `z`
    public Point3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // metody typu getter odpowiedzialne za zwracanie wartości zmiennej: `x`, `y`, `z`
    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    // metodę `getXYZ` zwracającą współrzędne `x`, `y`, `z` w postaci tablicy trzyelementowej
    public float[] getXYZ() {
        return new float[]{x, y, z};
    }

    // metodę setXY ustawiającą współrzędne `x` i `y`, `z`

    // metodę `setXYZ` ustawiającą wartości dla zmiennych `x`, `y`, `z`
    public void setXYZ(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // metoda `toString` powinna zwracać łańcuch tekstowy o następującym formacie: `(x, y, z)`
    @Override
    public String toString() {
        return String.format("'(%.1f, %.1f, %.1f)'", x, y, z);
    }
}
