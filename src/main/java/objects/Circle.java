package objects;

/**
 * Zaimplementuj klasę `Circle` która będzie zawierać te same pola co klasa Shape, oraz następujące cechy:
 * * pole odpowiedzialne za przechowywanie wartości promienia
 * * konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false` oraz pola `radius` na `1`
 * * konstruktor przyjmujący parametry `color`, `isFilled`, `radius`
 * * metodę typu `getter` odpowiedzialną za zwracanie wartości pola `radius`
 * * metodę typu `setter` odpowiedzialną za ustawianie wartości pola `radius`
 * * metodę `getArea` odpowiedzialną za obliczanie pola powierzchni
 * * metodę `getPerimeter` odpowiedzialną za obliczanie obwodu
 * * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji:
 * `Circle with radius=? which is a subclass off y`, gdzie ? oznacza wartość promienia, a wartość `y`
 * powinna być rezultatem wywołania metody `toString` z klasu bazowej
 */

public class Circle extends Shape {
    // pole odpowiedzialne za przechowywanie wartości promienia
    private int radius;

    // konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false` oraz pola `radius` na `1`
    public Circle() {
        super();
        radius = 1;
    }

    // konstruktor przyjmujący parametry `color`, `isFilled`, `radius`
    public Circle(String color, boolean isFilled, int radius) {
        super(color, isFilled);
        this.radius = radius;
    }

    // metodę typu `getter` odpowiedzialną za zwracanie wartości pola `radius`
    public int getRadius() {
        return radius;
    }

    // metodę typu `setter` odpowiedzialną za ustawianie wartości pola `radius`
    public void setRadius(int radius) {
        this.radius = radius;
    }

    // metodę `getArea` odpowiedzialną za obliczanie pola powierzchni
    public float getArea() {
        return (float) (Math.PI * Math.pow(radius, 2));
    }

    // metodę `getPerimeter` odpowiedzialną za obliczanie obwodu
    public float getPerimeter() {
        return (float) (2 * Math.PI * radius);
    }

    /**
     * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji:
     * `Circle with radius=? which is a subclass off y`, gdzie ? oznacza wartość promienia, a wartość `y`
     * powinna być rezultatem wywołania metody `toString` z klasu bazowej
     */
    @Override
    public String toString() {
        String parentClass = super.toString();
        return String.format("Circle with radius = %d which is a subclass off %s", radius, parentClass);
    }
}
