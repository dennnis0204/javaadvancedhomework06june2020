package objects;

public class Point3DTest {
    public static void main(String[] args) {
        Point3D point3D = new Point3D(25, 34, 12);
        System.out.println(point3D);

        point3D.setXYZ(33.33f, 22.22f, 75);
        System.out.println(point3D);
    }
}
