package objects;

import java.util.Arrays;

public class LineTest {
    public static void main(String[] args) {
        Line line = new Line(2, 1, 6, 4);
        System.out.println(line.getLineSegmentLength());
        System.out.println(Arrays.toString(line.getMiddlePointOfLineSegment()));
    }
}
