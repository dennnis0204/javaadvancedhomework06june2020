package objects;

public class ShapeTest {
    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.setColor("green");
        shape.setFilled(true);
        System.out.println(shape);
    }
}
