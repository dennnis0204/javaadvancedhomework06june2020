package objects;

/**
 * Zaimplementuj klasę `Rectangle` która będzie zawierać te same pola co klasa Shape, oraz następujące cechy:
 * * pole szerokość oraz długość będące typem `double`
 * * konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false` oraz pola `width` i `length` na `1`
 * * konstruktor przyjmujący parametry `color`, `isFilled`, `width` i `length`
 * * metody typu `getter` do zwracania wartości pól `width`, `length`
 * * metody typu `setter` do ustawiania wartości pól `width` i `length`
 * * metodę `getArea` odpowiedzialną za obliczanie pola powierzchni
 * * metodę `getPerimeter` odpowiedzialną za obliczanie obwodu
 * * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji:
 * `Rectangle with width=? and length=? which is a subclass off y`, gdzie ?
 * oznacza wartość odpowiednio szerokości i długości, a wartość `y` powinna być
 * rezultatem wywołania metody `toString` z klasy bazowej
 */

public class Rectangle extends Shape {
    // pole szerokość oraz długość będące typem `double`
    private double width;
    private double length;

    // konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false` oraz pola `width` i `length` na `1`
    public Rectangle() {
        super();
        this.width = 1;
        this.length = 1;
    }

    // konstruktor przyjmujący parametry `color`, `isFilled`, `width` i `length`
    public Rectangle(String color, boolean isFilled, double width, double length) {
        super(color, isFilled);
        this.width = width;
        this.length = length;
    }

    // metody typu `getter` do zwracania wartości pól `width`, `length`
    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    // metody typu `setter` do ustawiania wartości pól `width` i `length`
    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    // metodę `getArea` odpowiedzialną za obliczanie pola powierzchni
    public double getArea() {
        return width * length;
    }

    // metodę `getPerimeter` odpowiedzialną za obliczanie obwodu
    public double getPerimeter() {
        return 2 * (width + length);
    }

    /**
     * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji:
     * `Rectangle with width=? and length=? which is a subclass off y`, gdzie ?
     * oznacza wartość odpowiednio szerokości i długości, a wartość `y` powinna być
     * rezultatem wywołania metody `toString` z klasy bazowej
     */
    @Override
    public String toString() {
        String parentClass = super.toString();
        return String.format("Rectangle with width = %.1f  and length = %.1f which is a subclass off %s", width, length, parentClass);
    }


}