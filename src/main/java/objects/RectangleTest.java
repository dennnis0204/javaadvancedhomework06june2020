package objects;

public class RectangleTest {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle("red", true, 4, 5);
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle);
    }
}
