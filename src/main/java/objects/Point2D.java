package objects;

/**
 * ## Zadanie 1
 * ### Klasa Point2D
 * Zaimplementuj klasę `Point2D`. Klasa powinna zawierać:
 * * dwa pola typu float: `x`, `y`
 * * konstruktor bezparametrowy ustawiający wartość pól `x` i `y` na `0`
 * * konstruktor z dwoma parametrami: `float x`, `float y`
 * * metody typu getter odpowiedzialne za zwracanie wartości zmiennej: `x`, `y`
 * * metodę getXY zwracającą współrzędne x i y w postaci tablicy dwuelementowej
 * * metody typu setter odpowiedzialne za ustawianie wartości pól `x`, `y`
 * * metodę setXY ustawiającą współrzędne `x` i `y`
 * * metoda `toString` powinna zwracać łańcuch tekstowy o następującym formacie: `(x, y)`;
 *
 * Zaprezentuj zaimplementowane powyżej rozwiązanie na przykładzie.
 */

public class Point2D {
    // dwa pola typu float: `x`, `y`
    private float x;
    private float y;

    // konstruktor bezparametrowy ustawiający wartość pól `x` i `y` na `0`
    public Point2D() {
    }

    // konstruktor z dwoma parametrami: `float x`, `float y`
    public Point2D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    // metody typu getter odpowiedzialne za zwracanie wartości zmiennej: `x`, `y`
    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    // metodę getXY zwracającą współrzędne x i y w postaci tablicy dwuelementowej
    public float[] getXY() {
        return new float[]{x, y};
    }

    // metody typu setter odpowiedzialne za ustawianie wartości pól `x`, `y`
    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    // metodę setXY ustawiającą współrzędne `x` i `y`
    public void setXY(float x, float y) {
        this.x = x;
        this.y = y;
    }

    // metoda `toString` powinna zwracać łańcuch tekstowy o następującym formacie: `(x, y)`
    @Override
    public String toString() {
        return String.format("'(%.1f, %.1f)'", x, y);
    }
}
