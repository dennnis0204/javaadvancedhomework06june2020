package objects;

/**
 * Zaimplementuj klasę `Shape`. Klasa powinna zawierać:
 * * pole odpowiedzialne za przechowywanie koloru
 * * pole odpowiedzialne za przechowywanie informacji o tym czy kolor powinien wypełniać figurę czy nie
 * * konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false`
 * * konstruktor przyjmujący parametry `color` i `isFilled`
 * * metody typu `getter` odpowiedzialne za zwracanie wartości pól klasy
 * * metody typu `setter` odpowiedzialne za ustawianie wartości pól klasy
 * * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji:
 * `Shape with color of ? and filled/NotFilled`, gdzie ? oznacza wartość kolor, a wartość `filled`/`not filled`
 * powinna zostać zwracana w zależności od pola `isFilled`
 */

public class Shape {
    // pole odpowiedzialne za przechowywanie koloru
    private String color;

    // pole odpowiedzialne za przechowywanie informacji o tym czy kolor powinien wypełniać figurę czy nie
    private boolean isFilled;

    // konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false`
    public Shape() {
        color = "unknown";
        isFilled = false;
    }

    // konstruktor przyjmujący parametry `color` i `isFilled`
    public Shape(String color, boolean isFilled) {
        this.color = color;
        this.isFilled = isFilled;
    }

    // metody typu `getter` odpowiedzialne za zwracanie wartości pól klasy
    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return isFilled;
    }

    // metody typu `setter` odpowiedzialne za ustawianie wartości pól klasy
    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        isFilled = filled;
    }

    /**
     * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji:
     * `Shape with color of ? and filled/NotFilled`, gdzie ? oznacza wartość kolor, a wartość `filled`/`not filled`
     * powinna zostać zwracana w zależności od pola `isFilled`
     */
    @Override
    public String toString() {
        String filledMessage = isFilled ? "filled" : "not filled";
        return String.format("Shape with color of %s and %s.", color, filledMessage);
    }
}
